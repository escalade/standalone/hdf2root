#ifndef HDF2ROOT
#define HDF2ROOT

#include "H5Cpp.h"
#include <TTree.h>
#include <vector>

using namespace H5;

int help();

char getType(const AbstractDs &d);

TObject *createScalar(TString objName, const DataSet &dataSet);
TObject *createObject(TString, const H5Object&, Bool_t = kFALSE, int = 0);
TObject *createAttribute(TString objName, const Attribute *attr);

void reversePtr(hsize_t* dim, int rank);
void payload(Group *group, Bool_t bSequential = kFALSE, int bVerbose = 0);

#endif

