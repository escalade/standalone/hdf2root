# - Check for the presence of HDF5
#
# The following variables are set when HDF5 is found:
#  HDF5_FOUND       = Set to true, if all components of HDF5
#                          have been found.
#  HDF5_INCLUDE_DIRS   = Include path for the header files of HDF5
#  HDF5_LIBRARIES  = Link these to use HDF5

## -----------------------------------------------------------------------------
## Check for the header files

find_path (HDF5_INCLUDE_DIRS hdf5.h PATHS /usr/local/include /usr/include $ENV{HDF5}/include ${HDF5}/include)

## -----------------------------------------------------------------------------
## Check for the library

find_library (HDF5_LIBRARY   NAMES hdf5   PATHS  /usr/local/lib /usr/lib /lib $ENV{HDF5}/lib ${HDF5}/lib)
find_library (HDF5_LIBRARY_CPP NAMES hdf5_cpp PATHS  /usr/local/lib /usr/lib /lib $ENV{HDF5}/lib ${HDF5}/lib)
find_library (HDF5_LIBRARY_HL  NAMES hdf5_hl  PATHS  /usr/local/lib /usr/lib /lib $ENV{HDF5}/lib ${HDF5}/lib)
find_library (HDF5_LIBRARY_HL_CPP  NAMES hdf5_hl_cpp  PATHS  /usr/local/lib /usr/lib /lib $ENV{HDF5}/lib ${HDF5}/lib)
find_library (HDF5_LIBRARY_TOOLS  NAMES hdf5_tools  PATHS  /usr/local/lib /usr/lib /lib $ENV{HDF5}/lib ${HDF5}/lib)

set(HDF5_LIBRARIES ${HDF5_LIBRARY})
if(NOT ${HDF5_LIBRARY_CPP} STREQUAL "HDF5_LIBRARY_CPP-NOTFOUND")
  set(HDF5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_LIBRARY_CPP})
endif()
if(NOT ${HDF5_LIBRARY_HL} STREQUAL "HDF5_LIBRARY_HL-NOTFOUND")
  set(HDF5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_LIBRARY_HL})
endif()
if(NOT ${HDF5_LIBRARY_HL_CPP} STREQUAL "HDF5_LIBRARY_HL_CPP-NOTFOUND")
  set(HDF5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_LIBRARY_HL_CPP})
endif()
if(NOT ${HDF5_LIBRARY_TOOLS} STREQUAL "HDF5_LIBRARY_TOOLS-NOTFOUND")
  set(HDF5_LIBRARIES ${HDF5_LIBRARIES} ${HDF5_LIBRARY_TOOLS})
endif()

## -----------------------------------------------------------------------------
## Actions taken when all components have been found
if (HDF5_INCLUDE_DIRS AND HDF5_LIBRARIES)
  set (HDF5_FOUND TRUE)
else (HDF5_INCLUDE_DIRS AND HDF5_LIBRARIES)
  if (NOT HDF5_FIND_QUIETLY)
    if (NOT HDF5_INCLUDE_DIRS)
      message (STATUS "Unable to find HDF5 header files!")
    endif (NOT HDF5_INCLUDE_DIRS)
    if (NOT HDF5_LIBRARIES)
      message (STATUS "Unable to find HDF5 library files!")
    endif (NOT HDF5_LIBRARIES)
  endif (NOT HDF5_FIND_QUIETLY)
endif (HDF5_INCLUDE_DIRS AND HDF5_LIBRARIES)

if (HDF5_FOUND)

  if (NOT HDF5_FIND_QUIETLY)
    message (STATUS "Found components for HDF5")
    message (STATUS "HDF5_INCLUDE_DIRS = ${HDF5_INCLUDE_DIRS}")
    message (STATUS "HDF5_LIBRARIES    = ${HDF5_LIBRARIES}")
  endif (NOT HDF5_FIND_QUIETLY)
else (HDF5_FOUND)
  if (HDF5_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find HDF5!")
  endif (HDF5_FIND_REQUIRED)
endif (HDF5_FOUND)

mark_as_advanced (
  HDF5_FOUND
  HDF5_LIBRARIES
  HDF5_INCLUDE_DIRS
  )
